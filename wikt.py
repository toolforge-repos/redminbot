#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

from mw_api_client import wiki, Wiki
from time import sleep
from config import GRABBER_API_URL, USERAGENT, CREATOR_API_URL, PASSWORD, USERNAME
from sys import argv
from utils import performreplacements

grabber = Wiki(GRABBER_API_URL, USERAGENT)
creator = Wiki(CREATOR_API_URL, USERAGENT)

creator.clientlogin(USERNAME, PASSWORD)
cat = argv[1:][0] + ' given names'

i = 0
exists = open("cache/exists.txt", "a", encoding="utf-8") 
for entry in grabber.category(cat.removeprefix("'")).categorymembers():
    if i < 14:
        if str(entry.title) not in open('cache/exists.txt', 'r', encoding='utf-8').read():
                #Skip categories
                if str(entry.title).startswith('Category:') is False:
                    try:
                        creator.page(grabber.page(entry).title).edit(performreplacements(entry.read()), '+', createonly=True)
                        exists.write('\n' + grabber.page(entry).title)
                        i = i + 1
                    except (wiki.WikiError):
                        exists.write('\n' + grabber.page(entry).title)
    else:
        sleep(35)
        i = 0
        
exists.close()
print('Done!')