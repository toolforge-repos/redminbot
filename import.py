from mw_api_client import wiki, Wiki
from utils import performreplacements
from config import USERAGENT2, USERNAME, GRABBER_API_URL, CREATOR_API_URL, PASSWORD
grabber = Wiki(GRABBER_API_URL, USERAGENT2)
creator = Wiki(CREATOR_API_URL, USERAGENT2)
from sys import argv

creator.clientlogin(USERNAME, PASSWORD)

exists = open(argv[1], 'r', encoding='utf-8') 
ns = argv[2]
for page in exists.readlines():
    if 'sandbox' not in str(page):
        try:
            load = grabber.page(ns + page)
            title = load.title
            article = load.read()
            creator.page(title).edit(performreplacements(article), '[[en:' + title +']] থেকে আনা হলো', createonly=True)
            
        except (wiki.WikiError):
            print(page + ' already exists')
    else:
        print('Skipping ' + page)

exists.close()
