from mw_api_client import wiki, Wiki
from time import sleep
from config import USERAGENT, CREATOR_API_URL, PASSWORD, USERNAME
from utils import performreplacements

w = Wiki(CREATOR_API_URL, USERAGENT)
w.clientlogin(USERNAME, PASSWORD)
processed = open('cache/processed.txt', 'a', encoding='utf-8')
error = open('tmp/error.txt', 'a', encoding='utf-8')

i = 0
for page in w.allpages(limit='max', namespace=0):
    try:
        if i < 50:
            if str(page.title) not in open('cache/processed.txt', 'r', encoding='utf-8'):
                page.replace(page.read(), performreplacements(page.read()), summary='অনুবাদ')
                processed.write('\n' + str(page.title))
                i=i+1
        else:
            sleep(5)
            i = 0
    except(wiki.WikiError):
        print('Had to skip ' + str(page.title))
        processed.write('\n' + str(page.title))
    except(ValueError):
        print('Had to skip ' + str(page.title) + 'due to ValueError.')
        error.write('\n' + str(page.title))

processed.close()
error.close()
print('Done!')