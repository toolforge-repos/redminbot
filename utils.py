
#@TODO: This should be in a JSON file instead
def performreplacements(content):
    ret = str(content)
 #   ret = ret.replace('en-adj', 'en-বিশেষণ')
  #  ret = ret.replace('head|bn|proper noun|', 'head|bn|নামবাচক বিশেষ্য|')
  #  ret = ret.replace('Proper nouns', 'নামবাচক বিশেষ্য')
  #  ret = ret.replace('Proper noun', 'নামবাচক বিশেষ্য')
  #  ret = ret.replace('en-adv', 'en-ক্রিয়াবিশেষণ')
    ret = ret.replace('Noun', 'বিশেষ্য')
    ret = ret.replace('পুরুষ given name', 'পুরুষবাচক মূল নাম')
    ret = ret.replace('English', 'ইংরেজি')
    ret = ret.replace('Bengali', 'বাংলা')
    ret = ret.replace('Etymology', 'ব্যুৎপত্তি')
    ret = ret.replace('Alternative forms', 'বিকল্প বানান')
    ret = ret.replace('verb form', 'ক্রিয়ার রূপ')
  #  ret = ret.replace('lang|', 'ভাষা|')
    ret = ret.replace('{see আরও দেখুন', '{আরও দেখুন')
    ret = ret.replace('Adverb', 'ক্রিয়াবিশেষণ')
    ret = ret.replace('Adjective', 'বিশেষণ')
    ret = ret.replace('Verb', 'ক্রিয়া')
    ret = ret.replace('Pronunciation', 'উচ্চারণ')
    ret = ret.replace('Derived terms', 'উদ্ভূত শব্দ')
    ret = ret.replace('Female ', 'নারী ')
    ret = ret.replace('Male ', 'পুরুষ ')
    ret = ret.replace('Sanskrit', 'সংস্কৃত')
    ret = ret.replace('References', 'তথ্যসূত্র')
    ret = ret.replace('Arabic', 'আরবি')
    ret = ret.replace('Persian', 'ফার্সি')
    ret = ret.replace('Related terms', 'সম্পর্কিত পদ')
    ret = ret.replace('Ancient Greek', 'প্রাচীন গ্রিক')
    ret = ret.replace('Greek', 'গ্রিক')
    ret = ret.replace('Hinduism', 'হিন্দু ধর্ম')
    ret = ret.replace('Islam ', 'ইসলাম ')
    ret = ret.replace('ইসলামic,' 'Islamic')
    ret = ret.replace('Plants', 'গাছপালা')
    ret = ret.replace('Individuals', 'ব্যক্তি')
    ret = ret.replace('Hindi', 'হিন্দি')
    ret = ret.replace('Urdu', 'উর্দু')
    ret = ret.replace('Pali', 'পালি')
    ret = ret.replace('French', 'ফরাসি')
    ret = ret.replace('Italian', 'ইতালীয়')
    ret = ret.replace('Germanic', 'জার্মানীয়')
    ret = ret.replace('German', 'জার্মান')
    ret = ret.replace('Tamil', 'তামিল')
    ret = ret.replace('Latin', 'লাতিন')
 #   ret = ret.replace('|proper noun}', '|নামবাচক বিশেষ্য}')
  #  ret = ret.replace('|proper nouns}', '|নামবাচক বিশেষ্য}')
  #  ret = ret.replace('|proper nouns|', '|নামবাচক বিশেষ্য|' )
  #  ret = ret.replace('|proper noun|', '|নামবাচক বিশেষ্য|' )
  #  ret = ret.replace('|noun}', '|বিশেষ্য}')
    ret = ret.replace('Spanish', 'স্পেনীয়')
    ret = ret.replace('Portuguese', 'পর্তুগিজ')
    ret = ret.replace('Brazilian', 'ব্রাজিলীয়')
    ret = ret.replace('Romanian', 'রোমানীয়')
    ret = ret.replace('Catalan', 'কাতালান')
    ret = ret.replace('Danish', 'ড্যানিশ')
    ret = ret.replace('Faroese', 'ফ্যারো')
    ret = ret.replace('Polish', 'পোলিশ')
    ret = ret.replace('Hungarian', 'হাঙ্গেরীয়')
    ret = ret.replace('Estonian', 'এস্তোনিয়')
    ret = ret.replace('Indonesian', 'ইন্দোনেশীয়')
    ret = ret.replace('Finnish', 'ফিনিশ')
    ret = ret.replace('Further reading', 'আরো পড়ুন')
    ret = ret.replace('See also', 'আরো দেখুন')
    ret = ret.replace('Swedish', 'সুইডিশ')
    ret = ret.replace('Anagrams', 'অ্যানাগ্রাম')
    ret = ret.replace('Slovak', 'স্লোভাক')
    ret = ret.replace('Latvian', 'লাতভীয়')
    ret = ret.replace('Dutch', 'ওলন্দাজ')
    ret = ret.replace('Japanese', 'জাপানী')
    ret = ret.replace('Norwegian', 'নরওয়েজীয়')
    ret = ret.replace('Czech', 'চেক')
    ret = ret.replace('Tagalog', 'তাগালোগ')
    ret = ret.replace('Cebuano', 'সেবুয়ানো')
    ret = ret.replace('Audio', 'অডিও')
    ret = ret.replace('Esperanto', 'এসপেরান্তো')
    ret = ret.replace('Ilocano', 'ইলোকানো')
    ret = ret.replace('Albanian', 'আলবেনীয়')
    ret = ret.replace('Icelandic', 'আইসল্যান্ডীয়')
    ret = ret.replace('Lithuanian', 'লিথুয়ানীয়')
    ret = ret.replace('Munji', 'মুঞ্জি')
    ret = ret.replace('Ottoman Turkish', 'উসমানীয় তুর্কি')
    ret = ret.replace('Chagatai', 'চাগাতাই')
    ret = ret.replace('Turkic', 'তুর্কীয়')
    ret = ret.replace('Malay', 'মালয়')
    ret = ret.replace('Aramaic', 'আরামীয়')
    ret = ret.replace('Gujarati', 'গুজরাটি')
    ret = ret.replace('Yiddish', 'য়িদ্দিশ')
    ret = ret.replace('Punjabi', 'পাঞ্জাবি')
    ret = ret.replace('Grammar', 'ব্যাকরণ')
    ret = ret.replace('Hawaiian', 'হাওয়াইয়ান')
    ret = ret.replace('Korean', 'কোরীয়')
    ret = ret.replace('Irish', 'আইরিশ')
    ret = ret.replace('Marathi', 'মারাঠি')
    ret = ret.replace('Nepali', 'নেপালী')
    ret = ret.replace('Hausa', 'হাউসা')
    ret = ret.replace('Burmese', 'বর্মী')
    ret = ret.replace('singulars', 'একবচন')
    ret = ret.replace('plurals', 'বহুবচন')
    ret = ret.replace('একবচনs', 'একবচন')
    ret = ret.replace('বহুবচনs', 'বহুবচন')
    ret = ret.replace('West', 'পশ্চিম')
    ret = ret.replace('Frisian', 'ফ্রিজিয়ান')
    ret = ret.replace('North', 'উত্তর')
    ret = ret.replace('South', 'দক্ষিণ')
    ret = ret.replace('East', 'পূর্ব')
    ret = ret.replace('Greenlandic', 'গ্রিনল্যান্ডীয়')
    ret = ret.replace('Javanese', 'জাভাই')
    ret = ret.replace('Serbo-Croatian', 'সার্বো-ক্রোয়েশীয়')
    ret = ret.replace('Turkish', 'তুর্কি')
    ret = ret.replace('Rivers', 'নদী')
    ret = ret.replace('{IPA', '{আধ্বব')
    ret = ret.replace('Romanian', 'রোমানীয়')
    ret = ret.replace('Levantine', 'লেভানটাইন')
    ret = ret.replace('{R:হিব্রু Terms Database', '{R:হিব্রু Terms Database')
    return ret