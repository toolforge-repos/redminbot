from requests import get

id = input('Query id:')
name = input('File name:')
response = get('https://quarry.wmcloud.org/run/' + id + '/output/0/csv')
with open(name, 'wb') as file:
    file.write(response.content)
print('Done')
